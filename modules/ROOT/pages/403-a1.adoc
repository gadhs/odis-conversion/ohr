= 403 A1 Medical and Physical Examination Job List
:policy-title: Medical and Physical Examination Job List
:policy-number: 403 A1
:revised-date: 06/08/2020
:next-review-date:
:release-date:

include::partial$appendix-header.adoc[]

[cols="1,4,1"]
|===
| Job Code | Job Title | MAPEP

| 40815
| Accountant
| 1

| 40814
| Accountant 1, Professional
| 1

| 40805
| Accountant 2, Professional
| 1

| 40804
| Accountant 3, Professional
| 1

| 40806
| Accountant, Paraprofessional
| 1

| 40807
| Accounting Clerk
| 1

| 40801
| Accounting Director 2
| 1

| 40803
| Accounting Manager 1
| 1

| 40802
| Accounting Manager 2
| 1

| 40215
| Accounting Manager, OCSS
| 1

| 71305
| Activity Therapist
| 2

| 71328
| Activity Therapist (CSH)
| 2

| 71315
| Activity Therapy Coordinator
| 2

| 71303
| Activity Therapy Leader
| 2

| 71319
| Activity Therapy Supervisor
| 2

| 60104
| Administrative Assistant
| 1

| 60044
| Administrative Coordinator, Statewide
| 1

| 40002
| Administrative Operations Coordinator 1
| 1

| 40001
| Administrative Operations Coordinator 2
| 1

| 40003
| Administrative Operations Manager
| 1

| 60187
| Administrative Operations Specialist
| 1

| 14227
| Adoptions Coordinator Team Leader, Regional
| 1

| 14222
| Adoptions Coordinator, Regional
| 1

| 14226
| Adoptions Placement Coordinator
| 1

| 14225
| Adoptions Policies & Contracts Manager
| 1

| 14223
| Adoptions Resources Recruitment Manager
| 1

| 71821
| Advocacy Compliance Training Director, Reg
| 1

| 14063
| Aging Services Coordinator
| 1

| 14053
| Aging Services Team Leader
| 1

| 80105
| Applications Systems Manager
| 1

| 60502
| Artist 2
| 1

| 60508
| Artist, Graphic
| 1

| 40038
| Assistant Director, DHS Office
| 1

| 19671
| Assistant Director, ORS
| 1

| 95014
| Attorney 1
| 1

| 95015
| Attorney 2
| 1

| 71326
| Audiologist (DHS)
| 1

| 60301
| Audio-Visual Program Supervisor
| 1

| 60303
| Audio-Visual Specialist 1
| 2

| 60302
| Audio-Visual Specialist 2
| 2

| 40407
| Audit Manager
| 1

| 40406
| Audit Supervisor
| 1

| 40404
| Auditor 1
| 1

| 40405
| Auditor 2
| 1

| 40403
| Auditor, Junior
| 1

| 15015
| Automotive Service Attendant
| 2

| 71001
| Autopsy Assistant (CSH)
| 4

| 70202
| Barber/Beautician
| 4

| 70203
| Barber/Beautician Supervisor
| 4

| 70904
| Behavior Specialist
| 4

| 70944
| Behavior Specialist (CSH)
| 4

| 70933
| Behavior Technician
| 4

| 40202
| Billing Clerk 2
| 1

| 40218
| Billing Services Manager, Regional
| 1

| 40627
| Budget & Planning Coordinator
| 1

| 40601
| Budget Administrator
| 1

| 40607
| Budget Analyst (Hospital)
| 1

| 40604
| Budget Analyst 1
| 1

| 40603
| Budget Analyst 2
| 1

| 40619
| Budget Analyst, Paraprofessional
| 1

| 40605
| Budget Assistant
| 1

| 40613
| Budget Director, Dept.
(DHS)
| 1

| 40602
| Budget Officer
| 1

| 40616
| Budget/Planning Administrator
| 1

| 80112
| Business Analyst
| 1

| 80111
| Business Analyst Associate
| 1

| 80113
| Business Analyst Supervisor
| 1

| 40023
| Business Manager, Hospital
| 1

| 50402
| Butcher
| 3

| 30201
| Carpenter
| 2

| 14101
| Caseworker, Public Health
| 1

| 20031
| Chief Engineer, DHS
| 1

| 14401
| Child Support Enforcement Agent
| 1

| 14407
| Child Support Enforcement Locate Technician
| 1

| 14404
| Child Support Enforcement Manager
| 1

| 14406
| Child Support Enforcement Regional Manager
| 1

| 14402
| Child Support Enforcement Supervisor
| 1

| 14005
| Children's Home Assistant Superintendent
| 1

| 14004
| Children's Home Superintendent
| 1

| 14415
| Claims Manager (DFCS)
| 1

| H6001
| Clerical Worker
| 1

| 60107
| Clerk 1, General
| 1

| 70706
| Client Trainer
| 1

| 71411
| Clinical Associate
| 4

| 71414
| Clinical Director
| 1

| 70408
| Clinical Lab Director, PH
| 1

| 70407
| Clinical Laboratory Aide
| 4

| 70406
| Clinical Laboratory Associate
| 4

| 70425
| Clinical Laboratory Director
| 4

| 70409
| Clinical Laboratory Manager
| 4

| 70412
| Clinical Laboratory Technician
| 4

| 70411
| Clinical Laboratory Technologist
| 4

| 70502
| Communicable Disease Specialist
| 4

| 70501
| Communicable Disease Supervisor
| 4

| 70503
| Communicable Disease Team Supervisor
| 4

| 81121
| Communications Equipment Officer
| 1

| 61502
| Community Resource Dev.
Dir./Atl Reg
| 1

| 60717
| Compliance Analyst
| 1

| 60728
| Compliance Review Team Leader
| 1

| 60718
| Compliance Reviewer
| 1

| 18004
| Construction & Real Property Supervisor.
| 1

| 18007
| Construction and Real Property Manager
| 1

| 18005
| Construction/Real Property Specialist
| 1

| 70954
| Consumer Specialist (DHS)
| 1

| 95101
| Contract Specialist 1
| 1

| 95102
| Contract Specialist 2
| 1

| 95118
| Contracts Liaison, Human Service
| 1

| 95121
| Contracts Manager, Human Service
| 1

| 95117
| Contracts Specialist, Regional (DHS)
| 1

| 95122
| Contracts Technician, Human Service
| 1

| 95123
| Contracts/Operations Coordinator
| 1

| 60933
| Correspondence/Information Specialist
| 1

| 14513
| Counselor, Substance Abuse
| 4

| 14062
| County Director 5
| 1

| 60605
| Courier, Pharmacy
| 2

| 31917
| Courier, Stock Worker
| 2

| 30003
| Craftsman, General Trades
| 2

| 14408
| CSE Review/Modification Agent
| 1

| H3002
| Custodial Services Worker
| 2

| 80015
| Data Management Specialist
| 1

| 80086
| Data Manager (DHS)
| 1

| 80502
| Data Transcriber 1
| 1

| 80503
| Data Transcriber 2
| 1

| 80504
| Data Transcriber Supervisor
| 1

| 80114
| Database Administrator
| 1

| 70305
| Dental Assistant
| 4

| 70315
| Dental Director, District
| 4

| 70307
| Dental Director, Institutional
| 4

| 70316
| Dental Health Program Director, PHSO
| 1

| 70318
| Dental Hygienist (CSH)
| 4

| 70302
| Dental Hygienist, Institutional
| 4

| 70303
| Dental Hygienist, Public Health
| 4

| 70319
| Dental Technician
| 4

| 70304
| Dentist, Institutional
| 4

| 14118
| Deputy County Administrator, Fulton
| 1

| 14058
| Deputy Director, Aging
| 1

| 14437
| Deputy Director, Child Support Enforcement
| 1

| 70882
| Deputy Superintendent
| 1

| 60503
| Desktop Publishing/Forms Design Specialist
| 1

| 70816
| Developmental Disabilities Chief
| 1

| 14606
| DFCS Community Resource Specialist
| 1

| 14034
| DFCS County Admin.
(Fulton)
| 1

| 14037
| DFCS County Director 1
| 1

| 14038
| DFCS County Director 2
| 1

| 14039
| DFCS County Director 3
| 1

| 14032
| DFCS County Director 4
| 1

| 14036
| DFCS Deputy County Director 4
| 1

| 14035
| DFCS Deputy County Director 5
| 1

| 14439
| DFCS Economic Support Screener
| 1

| 14117
| DFCS Field Director
| 1

| 14011
| DFCS Program Consultant
| 1

| 14056
| DFCS Section Director 2
| 1

| 14055
| DFCS Section Manager 1
| 1

| 14006
| DFCS Services Generalist
| 1

| 16927
| DFCS Training Project Leader
| 1

| 14012
| DFCS Unit Chief
| 1

| 14059
| DHS Council Director
| 1

| 50503
| Dietary Services Director
| 1

| 50506
| Dietetic Program Manager, Clinical
| 1

| 50501
| Dietetic Technician
| 3

| 50502
| Dietitian, Clinical
| 1

| 50504
| Dietitian, Clinical (CSH)
| 1

| 61716
| Director 1
| 1

| 17332
| Director Assistant, Office of Fraud and Abuse
| 1

| 10016
| Director, Dev.
Dis.
Training & Work Therapy
| 1

| 40418
| Director, DHS Office of Audits
| 1

| 14213
| Director, Office of Adoptions (DHS)
| 1

| 19672
| Director, Office of Fraud and Abuse
| 1

| 16914
| Director, Office of HR and Organizational Dev
| 1

| 14417
| Disability Adjudicator (DFCS)
| 1

| 70802
| District Health Coordinator
| 1

| 71415
| District Health Director
| 1

| 70806
| District Health Program Manager
| 1

| 14025
| Div of Aging Svcs Program Administrator
| 1

| 14041
| Div of Aging Svcs Program Consultant
| 1

| 14024
| Div of Aging Svcs Program Manager
| 1

| 70841
| Division Chief (CSH)
| 1

| 14054
| Division Deputy Director
| 1

| 20537
| Drafter
| 1

| 61071
| DUI Program Coordinator
| 1

| 14416
| Economic Support Administrator
| 1

| 80406
| EDP Process Control Tech.
2
| 1

| 10004
| Education Supervisor
| 1

| 70418
| EEG/EKG Technician
| 4

| 30401
| Electrician 1
| 2

| 70403
| Electrocardiograph Technician
| 4

| 70404
| Electroencephalograph Technician
| 4

| 70415
| Electroencephalograph Technician Supervisor
| 4

| 30504
| Electronic Equipment Technician
| 2

| 31514
| Elevator Specialist
| 2

| 14473
| Eligibility Program Director (FCDFCS)
| 1

| 70606
| Emergency Medical Technician (CSH)
| 4

| 70603
| Emergency Medical Technician (DHS)
| 4

| 70602
| EMS Coordinator, District
| 1

| 80001
| Engineer, Manager Enterprise Network
| 1

| 30312
| Engineering Project Manager
| 1

| T2001
| Engineering Services Worker
| 1,2*

| 20705
| Environmental Engineer, PH State Office
| 2

| 19427
| Environmental Health District Director
| 1

| 19432
| Environmental Health Officer Admin
| 1

| 19423
| Environmental Health Spec.
1 (County PH)
| 2

| 19424
| Environmental Health Spec.
2
| 2

| T1901
| Environmental Safety Worker
| 1,2*

| 90013
| Epidemiologist 1
| 4

| 90014
| Epidemiologist 2
| 4

| 90015
| Epidemiologist 3
| 4

| 90016
| Epidemiologist, Chief
| 4

| 90017
| Epidemiologist, Medical
| 4

| 14414
| Error Control Specialist
| 1

| 19658
| Evaluation and Monitoring Coordinator
| 1

| T3001
| Facilities Management Worker
| 1,2*

| 17413
| Facilities Police Captain
| 5

| 17409
| Facilities Police Corporal
| 5

| 17412
| Facilities Police Lieutenant
| 5

| 17411
| Facilities Police Sergeant
| 5

| 30047
| Facilities Support Svcs Director
| 1

| 14114
| Family Connections Case Manager
| 1

| 14113
| Family Connections Coordinator
| 1

| 14456
| Family Indep Case Manager Associate
| 1

| 14412
| Family Independence Case Manager 1
| 1

| 14454
| Family Independence Case Manager 2
| 1

| 14413
| Family Independence Case Mgt.
Supervisor
| 1

| 14409
| Family Independence Program Director
| 1

| 14108
| Family Service Worker 1
| 2

| 14107
| Family Service Worker 2
| 2

| 40042
| Financial Project Director
| 1

| 40015
| Financial Services Manager, Fulton DFCS
| 1

| 40027
| Financial Services Manager, Regional Facilities
| 1

| 40028
| Financial Services Supervisor
| 1

| H4001
| Financial Services Worker
| 1

| T4001
| Financial Services Worker
| 1

| 17442
| Fire and Safety Director
| 5

| 17309
| Firefighter
| 5

| 17313
| Firefighter Captain
| 5

| 17312
| Firefighter Lieutenant
| 5

| 17311
| Firefighter Sergeant
| 5

| 95124
| Fiscal Contracts Administrator (Aging)
| 1

| 95027
| Fiscal Contracts Manager
| 1

| 60179
| Fleet Specialist, DHS
| 1

| 30907
| Floor Care/Project Specialist
| 2

| T5001
| Food Management Worker
| 3

| 50305
| Food Service Director
| 3

| 50307
| Food Service Director (DHS)
| 3

| 50301
| Food Service Employee 1
| 3

| 50302
| Food Service Employee 2
| 3

| 50303
| Food Service Manager
| 3

| 50304
| Food Service Supervisor
| 3

| H5001
| Food Services Worker
| 3

| 70839
| Forensic Services Supervisor
| 4

| 70807
| Forensic Services Technician 1
| 4

| 70808
| Forensic Services Technician 2
| 4

| 70948
| Forensic Specialist
| 4

| 17487
| Fraud Investigation Unit Supervisor (FCDFCS)
| 1

| 14411
| Fraud Prevention Investigator
| 1

| 17485
| Fraud Prevention Investigator (Fulton)
| 1

| 40615
| Fund Source Manager
| 1

| 70007
| Graduate Intern
| 1,2,3,4*

| 41121
| Grants & Contracts Manager
| 1

| 41104
| Grants Administrator
| 1

| 41107
| Grants Program Consultant
| 1

| 30701
| Grounds Maintenance Director
| 1

| 30702
| Grounds Maintenance Manager
| 2

| 30704
| Groundskeeper
| 2

| 30705
| Groundskeeping Supervisor
| 2

| 81132
| GSAMS Coordinator
| 1

| T7002
| Health Care Assistant
| 1,2,4*

| H7001
| Health Care Worker
| 4

| 70704
| Health Education Consultant, PHSO
| 1

| 70817
| Health Information Mgt Director, CSH
| 1

| 70832
| Health Services Program Supervisor
| 1

| 70833
| Health Services Technician 1 (I/S)
| 4

| 70834
| Health Services Technician 2 (I/S)
| 4

| 80701
| Help Desk Support Specialist
| 1

| 80518
| HMIS Technician
| 1

| 71321
| Horticultural Therapist
| 2

| 30029
| Hospital Operation Support Director Reg
| 1

| 70848
| Hospital Clinical Services Coordinator
| 1

| 80153
| Hospital Information Services Director
| 1

| 80008
| Hospital Information Systems Director
| 1

| 31101
| Hospital Property Control Officer
| 2

| 70884
| Hospital Superintendent/Chief Facility Admin
| 1

| 30031
| Hospital Support Svcs Manager (Grwd)
| 1

| 30901
| Housekeeper
| 2

| 30908
| Housekeeping Assistant Director
| 2

| 30905
| Housekeeping Director (Hospital)
| 1

| 30903
| Housekeeping Manager
| 2

| 30902
| Housekeeping Team Leader
| 2

| 70201
| Houseparent
| 4

| 16071
| HRM Planner
| 1

| 60043
| Human Resources Admin Serv Supervisor
| 1

| 16026
| Human Resources Assistant
| 1

| 14652
| Human Services Technician (Houseparent)
| 1

| 30801
| HVAC Repair Technician
| 2

| 80123
| Information Systems Agency Team Leader
| 1

| 80124
| Information Systems Agency Team Manager
| 1

| 80122
| Information Systems Unit Chief, DHS
| 1

| T8001
| Information Systems Worker
| 1

| 80076
| Information Tech Section Mgr
| 1

| 16903
| Information Tech Training Specialist
| 1

| 80804
| Information Technology Coordinator, DHS
| 1

| 60904
| Institution Communications Director
| 1

| 19812
| Institution Safety Manager
| 2

| 10012
| Instructional Aide
| 1

| 10006
| Instructor 1
| 1

| 10005
| Instructor 2
| 1

| 10007
| Instructor 3
| 1

| 11405
| Instructor, Technical
| 1

| 11408
| Instructor, Vocational
| 1

| 19663
| Interagency Initiatives Coordinator
| 1

| 60056
| Interagency Program Manager
| 1

| 14000
| Interpreter, DHS
| 4

| 14001
| Interpreter, Public Health
| 4

| 17468
| Investigations Specialist, DHS
| 1

| 17341
| Investigator (CSH)
| 1

| 17405
| Investigator (DHS)
| 1

| 17404
| Investigator 2 (DHS)
| 1

| 17470
| Investigator, WIC
| 2

| 17403
| Investigator-in-Charge (DHS)
| 1

| H3001
| Labor Trades Worker
| 2

| T9001
| Laboratory Services Worker
| 4

| 80002
| LAN Engineer
| 1

| 30703
| Landscape Gardener
| 2

| 31305
| Laundry Director
| 1

| 31304
| Laundry Manager
| 2

| 31303
| Laundry Supervisor
| 2

| 31302
| Laundry Worker
| 2

| T1701
| Law Enforcement Worker
| 1,2,5*

| 18303
| Leasing Unit Supervisor, DHS
| 1

| 95406
| Legal Assistant
| 1

| 95413
| Legal Secretary (CSE)
| 1

| 95231
| Legal Services Manager
| 1

| 95209
| Legal Services Officer (DHS)
| 1

| 95506
| Legislative Coordinator
| 1

| 10703
| Librarian 1
| 1

| 10704
| Librarian 2
| 1

| 31306
| Linen Worker (Hospital)
| 2

| 31515
| Locksmith
| 2

| 60601
| Mail Services Clerk
| 2

| 60602
| Mail Services Lead Worker
| 2

| 30009
| Maintenance Assistant Director
| 1

| 30036
| Maintenance Director (CSH)
| 1

| 30011
| Maintenance Director (DHS)
| 1

| 30015
| Maintenance Engineer
| 2

| H3003
| Maintenance Worker
| 2

| 50401
| Meat Processing Plant Manager
| 3

| 32003
| Mechanic
| 2

| 32035
| Mechanic (CSH)
| 2

| 32005
| Mechanic Foreman
| 2

| 32007
| Mechanic Shop Supervisor
| 2

| 14405
| Medicaid Eligibility Specialist (DFCS)
| 1

| 70421
| Medical Assistant, Clinical (CSH)
| 4

| 70835
| Medical Record Info Technician
| 1

| 70825
| Medical Records Director
| 1

| 70813
| Medical Transcriptionist
| 1

| H7002
| Mental Health Worker
| 2

| 40046
| MH Regional Admin Manager
| 1

| 70822
| MH/MR Service Director 1
| 1

| 70821
| MH/MR Service Director 2
| 1

| 70815
| MH/MR Shift Supervisor
| 4

| 71146
| MH/MR Shift Supervisor (LPN)
| 4

| 70823
| MH/MR Team Leader
| 4

| 70837
| MH/MR Team Leader (CSH)
| 4

| 71147
| MH/MR Team Leader (RN)
| 4

| 19668
| MHMRSA Certification Review Manager
| 1

| 19625
| MHMRSA Assoc.
Director Regional Mgmt
| 1

| 19627
| MHMRSA Certification Reviewer
| 1

| 60018
| MHMRSA Consumer Assistance Coordinator
| 1

| 60016
| MHMRSA Consumer Protection & Per Imp Coord
| 1

| 70885
| MHMRSA Deputy Director
| 1

| 95126
| MHMRSA Dir Fac Sys Risk Mgt
| 1

| 70856
| MHMRSA Facility Administrator
| 1

| 71416
| MHMRSA Medical Director/Medical Sup
| 1

| 61049
| MHMRSA Methadone Services Coordinator
| 1

| 61718
| MHMRSA Policy Coordinator
| 1

| 70858
| MHMRSA Prevention Specialist
| 1

| 61047
| MHMRSA Program Consultant
| 1

| 61062
| MHMRSA Program Specialist
| 1

| 61072
| MHMRSA Project Specialist
| 1

| 41009
| MHMRSA Reg Medicaid Fin Spec
| 1

| 70881
| MHMRSA Regional Executive Director
| 1

| 16025
| MHMRSA Regional Fac Asst HR Dir
| 1

| 16081
| MHMRSA Regional Facilities HR Dir
| 1

| 95013
| MHMRSA Regional Risk Management
| 1

| 70876
| MHMRSA Regional Utilization Manager
| 1

| 17494
| MHMRSA Risk Mgt Invest Coordinator
| 1

| 70887
| MHMRSA Section Director
| 1

| 80006
| Micro Systems Support Specialist
| 1

| 80016
| Micro Systems Support Technician
| 1

| 80507
| MIS Field Support Desk Supervisor
| 1

| 30037
| Motor Pool/Mechanic Shop Manager
| 2

| 15009
| Motor Transport Dispatcher
| 1

| 15011
| Motor Transport Supervisor
| 1

| 80018
| Network Administration Team Leader (DHS)
| 1

| 80004
| Network Administrator
| 1

| 80003
| Network Specialist
| 1

| 70416
| Nuclear/Ultrasound Radiology Technologist
| 4

| 71128
| Nurse
| 4

| 71113
| Nurse (Inpatient)
| 4

| 71103
| Nurse Clinical Specialist
| 4

| 71135
| Nurse Consultant, PH State Office
| 4

| 71157
| Nurse Coordinator, OHIS (CSH)
| 4

| 71162
| Nurse Coordinator, Patient & Family Education
| 4

| 71119
| Nurse Coordinator, PH
| 4

| 71102
| Nurse Day Administrator (Savannah)
| 4

| 71114
| Nurse Executive (Hospital)
| 1

| 71115
| Nurse Executive, Associate (Hospital)
| 4

| 71169
| Nurse Executive, Reg Facilities
| 1

| 71108
| Nurse Infection Control (Inpatient)
| 4

| 71133
| Nurse Manager
| 4

| 71112
| Nurse Manager (Inpatient)
| 4

| 71117
| Nurse Manager, County
| 4

| 71111
| Nurse Night/Evening Administrator
| 4

| 71123
| Nurse Practitioner
| 4

| 71132
| Nurse Specialist
| 4

| 71121
| Nurse Specialist, PH
| 4

| 19609
| Nurse Surveyor (ORS)
| 4

| 16902
| Nurse Trainer
| 1

| 71158
| Nurse, Agricultural Occupational Health
| 4

| 71168
| Nurse, Assistant Chief (PHSO)
| 4

| 71137
| Nurse, Camp
| 4

| 71101
| Nurse, Charge (Inpatient)
| 4

| 71134
| Nurse, Chief
| 1

| 71156
| Nurse, Health Clinic Manager (Gracewood)
| 4

| 71109
| Nurse, Licensed Practical (Inpatient)
| 4

| 71129
| Nurse, Licensed Practical (LPN)
| 4

| 71125
| Nurse, Licensed Practical PH
| 4

| 71122
| Nurse, Public Health
| 4

| 71155
| Nurse, Sterile Supply Supervisor (CSH)
| 4

| 71116
| Nursing & Clinic Director, District
| 1

| 71131
| Nursing & Clinical Assistant Director, District PH
| 4

| 71142
| Nursing Assistant Lead, Certified
| 4

| 71141
| Nursing Assistant, Certified
| 4

| 71167
| Nursing Program Evaluator (PHSO)
| 4

| 71118
| Nursing Supervisor, PH
| 4

| 71606
| Nutrition Assistant
| 1

| 71607
| Nutrition Manager
| 1

| 71608
| Nutrition Program Consultant
| 1

| 71604
| Nutrition Program Manager
| 1

| 71601
| Nutrition Services Director
| 1

| 71602
| Nutritionist
| 1

| 71609
| Nutritionist, Chief
| 1

| 71603
| Nutritionist, Clinical
| 1

| 71313
| Occupational Therapist
| 2

| 71312
| Occupational Therapy Dept.
Director
| 2

| 60111
| Office Assistant
| 1

| 60106
| Office Manager
| 1

| 14214
| Office of Adoptions Program Manager
| 1

| 80049
| Office of Information Technology, Asst Director
| 1

| 60162
| Office Services Supervisor (TRS)
| 1

| 48002
| Office Supervisor (REV)
| 1

| 16078
| OHRMD Section Manager
| 1

| 70828
| Operating Room Technician
| 4

| 60716
| Operations Analysis Manager
| 1

| 60712
| Operations Analysis Technician
| 1

| 60713
| Operations Analyst 1
| 1

| 60714
| Operations Analyst 2
| 1

| 60715
| Operations Analyst 3
| 1

| 40037
| Operations Manager
| 1

| 31114
| Operations Services Section Manager
| 1

| 60003
| Operations Support Coordinator
| 1

| 60002
| Operations Support Manager
| 1

| 60031
| Operations Support Manager
| 1

| 16904
| Organizational Development Consultant
| 1

| 16940
| Organizational Development Consultant 2
| 1

| 19661
| ORS CCL Applicant Services Director
| 1

| 19667
| ORS CCL Program Consultant
| 1

| 19654
| ORS Complaint Intake Surveyor (Nurse)
| 4

| 19638
| ORS Deputy Section Director
| 1

| 19657
| ORS Personal Care Home Program Administrator
| 1

| 19639
| ORS Regional Director
| 1

| 60065
| ORS Rules Coordinator
| 1

| 19673
| ORS Section Director
| 1

| 70106
| Orthotic Maintenance Technician
| 2

| 70105
| Orthotic Technician 1
| 2

| 70107
| Orthotic Technician 2
| 2

| 70929
| Outdoor Therapeutic Program, Counselor
| 4

| 70927
| Outdoor Therapy Program Camp Director
| 1

| 70928
| Outdoor Therapy Program Supervisor
| 4

| 31501
| Painter
| 2

| 14442
| Parent Locate Manager, State
| 1

| 13002
| Parks Manager
| 1

| 40203
| Patient Accounts Officer
| 1

| 40207
| Patient Accounts Technician (Hospital)
| 1

| 71338
| Patient Activities Supervisor
| 2

| 70705
| Patient/Family Educator
| 1

| 70838
| Patients Records Technician
| 1

| 41205
| Payroll Clerk
| 1

| 41204
| Payroll Paraprofessional
| 1

| 41203
| Payroll Supervisor 1
| 1

| 41202
| Payroll Supervisor 2
| 1

| 14008
| Personal Advocate
| 1

| 16003
| Personnel Analysis Section Manager
| 1

| 16008
| Personnel Analyst 1
| 1

| 16009
| Personnel Analyst 2
| 1

| 16011
| Personnel Analyst 3
| 1

| 16001
| Personnel Director
| 1

| 16002
| Personnel Director, Deputy
| 1

| 16609
| Personnel Hearing Representative
| 1

| 16006
| Personnel Manager
| 1

| 16004
| Personnel Officer
| 1

| 16005
| Personnel Officer, Assistant
| 1

| 16018
| Personnel Operations Section Mgr
| 1

| 16007
| Personnel Representative
| 1

| T1601
| Personnel Services Worker (Extended Svcs Wkr)
| 1,2,3,4,5*

| 16801
| Personnel Technician 1
| 1

| 16802
| Personnel Technician 2
| 1

| 16012
| Personnel Transactions Manager
| 1

| 40017
| PH District Administrator
| 1

| 70423
| PH Laboratory Service Manager
| 4

| 71202
| Pharmacist
| 1

| 71209
| Pharmacist (CSH)
| 1

| 19632
| Pharmacist Surveyor (ORS)
| 1

| 71205
| Pharmacist, Clinical
| 1

| 71208
| Pharmacist, Clinical (CSH)
| 1

| 71206
| Pharmacy Director
| 1

| 71212
| Pharmacy Director, Assistant
| 1

| 71204
| Pharmacy Director, PH
| 1

| 71207
| Pharmacy Director, PH (DHS)
| 1

| 71214
| Pharmacy Director, Reg Facilities
| 1

| 71203
| Pharmacy Manager
| 1

| 71201
| Pharmacy Technician
| 1

| 71211
| Pharmacy Technician (CSH)
| 1

| 70417
| Phlebotomist
| 4

| 60801
| Photographic Records Technician 1
| 2

| 60802
| Photographic Records Technician 2
| 2

| 60803
| Photographic Records Technician Supervisor
| 2

| 70818
| PHSO Asst.
Immunization Program Manager
| 1

| 70505
| PHSO Branch Director
| 1

| 70814
| PHSO Immunization Field Rep
| 1

| 70008
| PHSO Program Administrator
| 1

| 70002
| PHSO Program Consultant 1
| 1

| 70006
| PHSO Program Consultant 2
| 1

| 70001
| PHSO Program Director
| 1

| 71317
| Physical Therapist
| 2

| 71329
| Physical Therapy/Occupational Ther Technician
| 1

| 71401
| Physician
| 4

| 71412
| Physician, Administrative (PH)
| 1

| 61039
| Planner 1
| 1

| 61041
| Planner 2
| 1

| 60402
| Planner, Conference
| 1

| 60403
| Planner, Conference (DHS)
| 1

| 31601
| Plumber
| 2

| 17467
| Police Corporal (CSH)
| 5

| 60029
| Policy Administrator (DHS)
| 1

| 60034
| Policy Analyst 1 (DHS)
| 1

| 60053
| Policy, Records Mgt & ADA Coordinator (DHS)
| 1

| 14221
| Post Adoption Services Administrator
| 1

| 70854
| PRES Director (Gracewood)
| 1

| 70852
| Primary Care/Managed Care Coordinator
| 1

| 60806
| Print Shop Supervisor
| 2

| 60804
| Printing Equipment Operator 1
| 2

| 60805
| Printing Equipment Operator 2
| 2

| 31701
| Procurement & Services Officer 1
| 2

| 31721
| Procurement Assistant
| 1

| 31722
| Procurement Coordinator
| 1

| 31703
| Procurement Officer 1
| 1

| 31704
| Procurement Officer 2
| 1

| T1001
| Professional Education Worker
| 1,2*

| T7001
| Professional Health Care Worker
| 1,4*

| T1401
| Professional Social Services Worker
| 1,4*

| 60112
| Program Assistant (DHS)
| 1

| 60113
| Program Associate (DHS)
| 1

| 61083
| Program Director 1
| 1,2,3,4*

| 61084
| Program Director 2
| 1,2,3,4*

| 40422
| Program Evaluation Analyst
| 1

| 40423
| Program Evaluation Manager
| 1

| 61129
| Program Evaluator
| 1

| 61074
| Program Operations Manager
| 1

| 19664
| Program Policy Specialist
| 1

| 19665
| Program/Regulatory Consultant
| 1

| 80102
| Programmer Analyst 1
| 1

| 80103
| Programmer Analyst 2
| 1

| 80104
| Programmer Analyst 3
| 1

| 16087
| Project Administrator
| 1

| 60068
| Project Director 1
| 1

| 60069
| Project Director 2
| 1

| 60709
| Property Management Specialist, DHS
| 1

| 30014
| Property/Asset Manager, DHS
| 1

| 30001
| Property/Supply Supervisor 1
| 2

| 30002
| Property/Supply Supervisor 2
| 2

| 70941
| Psychologist
| 4

| 70943
| Psychologist (CSH)
| 4

| 70925
| Psychologist, Forensic
| 4

| 70902
| Psychology Services, Chief
| 1

| 70702
| Public Health Education Director
| 1

| 70701
| Public Health Educator
| 1

| 70851
| Public Health Planner
| 1

| 70811
| Public Health Technician
| 4

| 60968
| Public Information & Media Manager
| 1

| 61064
| Public Policy Coordinator (Dev Dis Council)
| 1

| 60969
| Public Relations & Information Coordinator
| 1

| 60909
| Public Relations & Information Program Manager
| 1

| 60907
| Public Relations & Information Specialist 1
| 1

| 60908
| Public Relations & Information Specialist 2
| 1

| 60944
| Public Relations Director
| 1

| 31738
| Purchasing Operations Specialist (DOT)
| 1

| 61136
| QA/Fraud Invest.
Section Manager (FCDFCS)
| 1

| 61105
| Quality Assurance Specialist (Dekalb)
| 1

| 61118
| Quality Assurance Specialist (DFCS)
| 1

| 61119
| Quality Assurance Specialist (Fulton)
| 1

| 61133
| Quality Assurance Supervisor (Fulton)
| 1

| 61113
| Quality Control Specialist
| 1

| 61114
| Quality Control Supervisor
| 1

| 61137
| Quality Improvement Division Coordinator
| 1

| 61102
| Quality Improvement Manager, Continuous
| 1

| 61132
| Quality Management Associate (Hosp)
| 1

| 61106
| Quality Management Coordinator (DHS)
| 1

| 61107
| Quality Management Director
| 1

| 70431
| Radiograph Development Technician
| 2

| 70402
| Radiology Technologist
| 4

| 70419
| Radiology Technologist Supervisor
| 4

| 60105
| Receptionist
| 1

| 60221
| Records Management Officer
| 1

| 60219
| Records Management Technician
| 1

| 30039
| Reg Fac Materials Mgt Director
| 1

| 16935
| Reg Fac Staff Dev & Training Director
| 1

| 40032
| Reg Facilities Finance Director
| 1

| 19817
| Regional Fac Safety Env Health Manager
| 1

| 61063
| Regional Transportation Coordinator
| 1

| 70932
| Residential Services Coordinator
| 4

| 70422
| Respiratory Therapist
| 4

| 41314
| Risk Management Coordinator, DHS
| 1

| 19815
| Safety and Accreditation Manager (SWSH)
| 2

| 15214
| Sanitation Equipment Operator (CSH)
| 2

| 31301
| Seamster
| 2

| 31307
| Seamster Supervisor
| 2

| 60101
| Secretary 1
| 1

| 60102
| Secretary 2
| 1

| 60103
| Secretary 3
| 1

| 60114
| Secretary, Executive (DHS)
| 1

| 95405
| Secretary, Legal
| 1

| H1701
| Security Personnel
| 2,4*

| 14046
| Senior Services Center Manager (Douglas DFCS)
| 1

| 60035
| Sexual Predator Registry Manager
| 1

| H6002
| Skilled Trades Worker
| 1,2*

| 14202
| Social Services Administrator
| 1

| H1401
| Social Services Aide
| 1,2*

| 14212
| Social Services Case Management Associate
| 1

| 14203
| Social Services Case Manager
| 1

| 14205
| Social Services Case Mgr, Advanced
| 1

| 70914
| Social Services Coordinator
| 4

| 70938
| Social Services Coordinator (Hosp)
| 1

| 70915
| Social Services Coordinator 1, Licensed
| 4

| 70912
| Social Services Coordinator 2
| 4

| 70913
| Social Services Coordinator 2, Licensed
| 4

| 14211
| Social Services Program Director
| 1

| 70939
| Social Services Program Manager LCSW
| 4

| 70945
| Social Services Provider (CSH)
| 4

| 70917
| Social Services Provider 1
| 4

| 70918
| Social Services Provider 1, Licensed
| 4

| 70908
| Social Services Provider 2
| 4

| 70916
| Social Services Provider 2, Licensed
| 4

| 70909
| Social Services Provider, Hospital
| 4

| 14204
| Social Services Specialist
| 1

| 14201
| Social Services Supervisor
| 1

| 70924
| Social Services Technician 1
| 4

| 70923
| Social Services Technician 2
| 4

| 70922
| Social Services Technician 3
| 4

| T1402
| Social Services Technician Worker
| 1,4*

| 70953
| Social Services Technician, Community 2
| 4

| 70911
| Social Services Technician, Hospital
| 4

| 14231
| Social Services Treatment Specialist
| 1

| 70903
| Social Services, Chief Hospital
| 1

| 14047
| Social Services/Substance Abuse Program Mgr
| 1

| 80052
| Software Support Specialist, DHS
| 1

| 80726
| Software Training & Support Mgr
| 1

| 30049
| Special Assistant, OTS
| 1

| 61024
| Special Transportation Coordinator, DHS
| 1

| 19643
| Specialist Surveyor (ORS)
| 4

| 71301
| Speech Language Pathologist
| 1

| 16923
| Staff Development/Training Coordinator (CSH)
| 1

| 16909
| Staff Development/Training Coordinator 1
| 1

| 16911
| Staff Development/Training Coordinator 2
| 1

| 19619
| Standards Surveyor (ORS)
| 1

| 61403
| Statistical Analyst 1
| 1

| 61402
| Statistical Analyst 2
| 1

| 61404
| Statistical Technician
| 1

| 61407
| Statistical Unit Chief, DFCS (DHS)
| 1

| 61401
| Statistics Team Leader
| 1

| 30802
| Steamplant Operator
| 2

| 30803
| Steamplant Supervisor
| 2

| 70827
| Sterile Supply Technician
| 4

| 70829
| Sterile Supply Technician 2
| 4

| 31902
| Stock Worker
| 2

| 61207
| Store Manager 1
| 2

| 31901
| Storekeeper
| 2

| 61004
| Strategic Planner 4
| 1

| 40012
| Superintendent Assistant, Administrative
| 1

| 19707
| Support Services Supervisor
| 2

| T6001
| Support Services Worker
| 1

| 80005
| Systems Analyst
| 1

| 80508
| Systems Support Analyst
| 1

| 10205
| Teacher (CSH)
| 1

| 10204
| Teacher (DHS)
| 1

| 16934
| Technical Recruiter
| 1

| 81106
| Telecommunications Analyst
| 1

| 81136
| Telecommunications Manager (DHS)
| 1

| 30004
| Trades Supervisor
| 2

| 16913
| Training & Development Manager
| 1

| 14605
| Training Instructor 1
| 1

| 14604
| Training Instructor 2
| 1

| 16095
| Training Operations & Facilities Mgr
| 1

| 16916
| Training Program Administrator
| 1

| 16917
| Training Program Manager (DJJ)
| 1

| 16908
| Training Specialist
| 1

| 61907
| Transportation Funds Coordinator
| 1

| 61082
| Transportation Program Specialist
| 1

| 60058
| Transportation Services Manager, DHS
| 1

| 40025
| Transportation Services Unit Chief, DHS
| 1

| T1501
| Transportation Services Worker
| 1,2*

| 15206
| Truck Driver, Tractor Trailer
| 2

| 31726
| Unit Chief, Info Tech Administrative
| 1

| 19626
| Unit Chief/Monitoring & Evaluation
| 1

| 30005
| Utility Worker, Skilled
| 2

| 70801
| Utilization Management Coordinator
| 1

| 19634
| Vector Control Specialist
| 2

| 19633
| Vector Control Supervisor
| 2

| H3004
| Vehicle Operator
| 1,2*

| 15204
| Vehicle Operator/Courier
| 2

| H1402
| Vocational Worker (Extended Services Worker)
| 1,2,3,4,5*

| 61501
| Volunteer Resources Coordinator
| 1

| 31904
| Warehouse Supervisor (DHS)
| 2

| 30601
| Water Treatment Plant Operator
| 2

| 30602
| Water Treatment Plant Supervisor
| 2

| 80805
| Webmaster
| 1

| 80809
| Webmaster Assistant
| 1

| 14663
| Work Therapist
| 1
|===
