= 1505 Mediation Procedure
:policy-title: Mediation Procedure
:policy-number: 1505
:release-date: December 14, 2010
:revised-date: June 11, 2020
:next-review-date:

include::partial$policy-header.adoc[]

The Mediation Procedure is established to provide an informal, non-adversarial process in which employees and supervisors involved in workplace disputes may meet with a mediator.
The intent is to resolve disputes between employees and/or supervisors by reaching a mutually satisfactory agreement.
Both classified and unclassified employees may participate in mediation.

Employees and supervisors are encouraged to contact the Office of Human Resources (OHR) to discuss issues, which may be appropriate for mediation.
Employees who are attempting to informally resolve disputes by requesting mediation prior to filing formal grievances should be mindful of filing deadlines within the grievance procedures.

== Section A: Eligible Mediation Issues

. Work-related disputes between colleagues or between employees and their supervisors may be appropriate for mediation.
The issues may relate to a specific occurrence or may be ongoing.

== Section B: Ineligible Mediation Issues

. Mediation is inappropriate in some matters including, but not limited to, the following:

.. Issues which are pending or have been concluded by direct appeal to the State Personnel Board, the Georgia Commission on Equal Opportunity, or through other administrative or judicial procedures.
.. Allegations of unlawful discrimination or sexual harassment.
.. Pending staff reduction or other related actions; and,
.. Actions involving separations, demotions, salary reductions or suspensions with or without pay.

== Section C: Initiating The Mediation Process

. A supervisor or other authorized official may request that a mediation session be scheduled to resolve disputes between coworkers.

. An employee may request that a mediation session be scheduled to resolve disputes with a coworker or supervisor.

. A supervisor may request that a mediation session be scheduled to resolve disputes with a subordinate employee.

. Mediation requests are to be submitted to OHR by using the Mediation Request Form (Attachment #1).

. OHR may initiate mediation when work-related disputes are brought to their attention and mediation is determined appropriate.

== Section D: Review and Notification

. OHR will review all requests to determine if mediation is appropriate.

. If it is determined that mediation is inappropriate, OHR will provide written notification with reasons for the denial within five (5) workdays.

. If mediation is determined appropriate, OHR will schedule the mediation session.

.. OHR will assign a mediator.
.. The mediator should be the Human Resource Manager for that particular unit.
They cannot be related to any person involved in the dispute or employed in the direct line of authority of any person involved in the dispute.
.. The employees and/or supervisors involved will be advised when a mediator has been assigned.
Written notice of the time and location of the session will also be provided.

== Section E: The Mediation Session

. The mediator will conduct the session and work directly with each participant.

. The mediation session will not be taped.

. Only the persons involved in the dispute and the mediator are to be present in the session unless the session is observed for training purposes.
Participation of third parties is prohibited.

. The mediator actively listens to the people involved to discover how they would like to resolve the issue.

. The mediator does not assign blame but helps the participants to agree upon a fair and reasonable solution.

. If an agreement is reached, the terms are written down by the mediator and signed by both participants.

.. The mediation settlement agreement cannot violate laws, Rules of the State Personnel Board or DHS policies.
.. All mediation participants must comply with the terms of the mediation agreement, unless modified by mutual consent.
.. The settlement agreement should be added to each employee's personnel file.

. If an agreement is not reached, the mediator will close the session.
The employee must resolve the issue through the appropriate formal process.

For additional information or assistance, please contact your local Human Resource Office, or email DHS-Policies@dhs.ga.gov.
