= 103 Smoking
:policy-title: Smoking
:policy-number: 103
:references: O.C.G.A. 16-12-2 — Smoking in Public Places + \
O.C.G.A. § 31-12A-1 et seq. — Georgia Smokefree Air Act of 2005
:release-date: August 16, 1999
:revised-date: March 30, 2020
:next-review-date:

include::partial$policy-header.adoc[]

The Department of Human Services (DHS) is a smoke free environment.
Smoking is, therefore, prohibited in all facilities either occupied or controlled by DHS and in vehicles owned by or assigned to DHS.
Smoking is also prohibited in employee's personal vehicles during work hours when clients or customers are being transported.

This smoking policy is applicable to all employees, clients, customers, vendors, and visitors to DHS facilities.

== Section A: Prohibitions

. Smoking is prohibited in all enclosed facilities of, including buildings owned, leased, or operated by, the State of Georgia, its agencies and authorities, and any political subdivision of the state, municipal corporation, or local board or authority created by general, local, or special Act of the General Assembly or by ordinance or resolution of the governing body of a county or municipal corporation individually or jointly with other political subdivisions or municipalities of the state.

. Except as otherwise specifically provided, smoking shall be prohibited in all enclosed areas within places of employment, including, but not limited to, common work areas, auditoriums, classrooms, conference and meeting rooms, private offices, elevators, hallways, medical facilities, cafeterias, employee lounges, stairs, restrooms, and all other enclosed facilities.

== Section B: Provisions

. Authorized officials are to ensure a smoke free work environment in all areas occupied by DHS.

. "No Smoking" signs are to be posted in conspicuous places to indicate that the DHS facility is a smoke free work environment.

. Employees may smoke only during their meal periods and a maximum of two discretionary 15-minute break periods.
Additional smoking time is not allowed.

== Section C: Compliance

. Employees are required to comply with the provisions of this policy.

. Supervisors are responsible for ensuring that employees do not smoke in prohibited areas or use work time for smoking purposes.

. Appropriate disciplinary action, up to and including separation, will be taken against employees who violate this policy.

For additional information or assistance, please contact your local Human Resource Office, or email DHS-Policies@dhs.ga.gov.
