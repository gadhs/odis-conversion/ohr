= 504 A5 Disclosure of Information Received from GCIC and/or FBI
:policy-title: Disclosure of Information Received from GCIC and/or FBI
:policy-number: 504 A5
:revised-date: 05/01/2020
:next-review-date: 04/30/2022

include::partial$appendix-header.adoc[]

== Employee Letter Sample

{startsb}Date]

{startsb}Name]

{startsb}Address]

Dear [Applicant/Employee Name],

A criminal history record check was conducted on you as a condition of employment with the Department of Human Services, [Insert DHS Organizational Unit].
Information received from the Georgia Crime Information Center (GCIC) and/or the Federal Bureau of Investigation (FBI) [Only include the applicable agency] indicates the following pending charge(s) and/or conviction(s) in your record:

[cols=4*^]
|===
|Crime
|Designate One: +
Pending Charge Or Conviction
|Date
|Location

||||
||||
||||
|===

In accordance with the Rules of the GCIC Council, this letter serves as notice that your separation from employment is based, in part, on the above information.

Sincerely,

Name +
Title

---

*Note to Hiring Officials:*

*The disclosure notice must be filed in a separate file, not in the employee's personnel file.
In addition to this disclosure notice, a separation letter must be issued to the employee.
The separation letter is to be filed in the employee's personnel file.*
