= 502 Classified Employee Working Test Period and Permanent Status
:policy-title: Classified Employee Working Test Period and Permanent Status
:policy-number: 502
:references: Rules of the State Personnel Board 478-1-.24 — Working Test Period and + \
Permanent Status
:release-date: December 22, 2010
:revised-date: June 14, 2023
:next-review-date: June 13, 2025

include::partial$policy-header.adoc[]

== Section A: General Provisions

. Working test period is a probationary period of employment for a classified employee during which the employee must demonstrate that they have the knowledge, ability, aptitude, and other necessary qualities to satisfactorily perform assigned duties.

. Once the employee successfully completes a working test period in a job, the employee gains permanent status in that job.
Permanent status grants the employee additional notice and appeal rights that are not required during working test period.

== Section B: Working Test Time Frames

. Working test period begins on the first day classified employees report to work in new positions.

. Working test period duration is six (6) months.

. The working test period shall be extended day for day by any time spent on leave with pay under the State Personnel Board Rule provisions for Special Injury Leave (Rules 478- 1-.16(8)(c) and 478-1-.16(8)(d)) or in non-pay status.
However, time spent in non-pay status for ordered uniformed service (as defined in the Uniformed Services Employment and Reemployment Rights Act) shall not extend the working test period.

== Section C: Midpoint Review

. Managers shall conduct a midpoint performance review for each employee serving a working test period.

. The midpoint review shall be presented to the employee within ten (10) calendar days of the date the employee completes one-half of the working test period or as near to that date as is practicable.

. The midpoint review shall include an evaluation of the employee's progress and recommendations, if any, for corrective action.

== Section D: Permanent Status

. It is the responsibility of managers to determine if employees on a working test period will be granted permanent status.

.. Permanent status is effective on the calendar date following completion of the working test period.
.. Permanent status will not be granted to a classified employee prior to the acquisition and submission of any required license or certification.

. If it is determined, prior to the completion of the working test period, that the employee is not to be granted permanent status, the Department of Human Services may:

.. Demote the employee to a classified position equivalent to the last permanent status position held,
.. Transfer the employee to a classified position for which the employee is qualified,
.. Separate the employee; or,
.. Pursue a voluntary agreement with the employee on movement to a suitable vacancy for which the employee is qualified.

. An employee who is not transferred, demoted, or separated prior to eligibility for permanent status shall acquire permanent status.

. If an employee is transferred or demoted to a classified position, the employee will be considered to have permanent status in the new job on the effective date of the action.

. An employee shall be notified in writing of failure to attain permanent status, but the action may not be appealed unless otherwise stipulated in the following State Personnel Board Rules: 478-1-.26 — Adverse Actions for Classified Employees and 478-1-.27 — Appeals and Hearings for Classified Employees.

For additional information or assistance, please contact your local Human Resources Representative.
