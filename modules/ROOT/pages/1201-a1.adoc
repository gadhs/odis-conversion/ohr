= 1201 A1 Code of Ethics for Government Service
:policy-title: Code of Ethics for Government Service
:policy-number: 1201 A1
:revised-date: 06/02/2020
:next-review-date:
:release-date:

include::partial$appendix-header.adoc[]

Any person in government service should:

[upperalpha]
. Put loyalty to the highest moral principles and to country above loyalty to persons, party, or government department.

. Uphold the Constitution, laws, and legal regulations of the United States and the State of Georgia and of all governments therein and never be a party to their evasion.

. Give a full day's labor for a full day's pay and give to the performance of his duties his earnest effort and best thought.

. Seek to find and employ more efficient and economical ways of getting tasks accomplished.

. Never discriminate unfairly by the dispensing of special favors or privileges to anyone, whether for remuneration or not; and never accept, for himself or his family, favors or benefits under circumstances which might be construed by reasonable persons as influencing the performance of his governmental duties.

. Make no private promises of any kind binding upon the duties of office, since a government employee has no private word which can be binding on public duty.

. Engage in no business with the government, either directly or indirectly, which is inconsistent with the conscientious performance of his governmental duties.

. Never use any information coming to him confidentially in the performance of governmental duties as a means for making private profit.

. Expose corruption wherever discovered.

. Uphold these principles, ever conscious that public office is a public trust.
